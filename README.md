# ulinux

My custom build of [archlinux](http://archlinux.org) fit for my needs.


## Building

If you're on arch linux and have `archiso` installed then run
`make build-iso`

If you have `docker` then just run `make` (you might need `sudo` for docker)
