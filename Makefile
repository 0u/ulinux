all: docker-build docker-build-iso

build-iso:
	cd ./archlive && ./build.sh -v

clean:
	rm -rf archlive/out archlive/work

docker-build:
	docker build . -t 0u/archiso

docker-build-iso:
	docker run -v $(shell pwd):/tmp/build -t 0u/archiso make -C /tmp/build build-iso

vagrant-iso:
	vagrant
